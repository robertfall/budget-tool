require 'rails_helper'

describe AddTransactionSuggestions do
  describe '.for_bank_transactions' do
    before :each do
      @account = BankAccount.create(name: 'Cheque')
      @account.bank_transactions.create([
        { processed_on: 10.days.ago, description: 'Old', amount: 9.99 },
        { processed_on: Date.today, description: 'Current', amount: 10 },
        { processed_on: 10.days.from_now, description: 'New', amount: 11.99 }
      ])
      
      @account.trans.create([
        { processed_on: 15.days.ago, description: 'Kinda Old', amount: 10 },
        { processed_on: 14.days.ago, description: 'Not Old', amount: 100 },
        { processed_on: 10.days.ago, description: 'Also Old', amount: 10 },
        { processed_on: 20.days.from_now, description: 'irrelavant', amount: 10000 },
        { processed_on: 10.days.from_now, description: 'Might be new', amount: 10 }
      ])

      @reconciled_transaction = @account.trans.create({ 
        processed_on: 10.days.from_now, 
        description: 'Already Taken', 
        amount: 11.99 
      })

      @reconciled_bank_transaction = @reconciled_transaction.create_bank_transaction({ 
        bank_account: @account,
        processed_on: 10.days.from_now, 
        description: 'Already Taken', 
        amount: 11.99 
      })
    end

    it 'provides relevant suggestions' do
      @bank_transactions = AddTransactionSuggestions.for_bank_transactions(@account.trans, @account.bank_transactions)

      @old = @bank_transactions.find {|bt| bt.description == 'Old'}
      expect(@old.suggestions.count).to eq 2

      @new = @bank_transactions.find {|bt| bt.description == 'New'}
      expect(@new.suggestions.count).to eq 1
    end
  end
  
  describe '.for_transactions' do
    before :each do
      @account = BankAccount.create(name: 'Cheque')
      @account.trans.create([
        { processed_on: 10.days.ago, description: 'Old', amount: 9.99 },
        { processed_on: Date.today, description: 'Current', amount: 10 },
        { processed_on: 10.days.from_now, description: 'New', amount: 11.99 }
      ])
      
      @account.bank_transactions.create([
        { processed_on: 15.days.ago, description: 'Kinda Old', amount: 10 },
        { processed_on: 14.days.ago, description: 'Not Old', amount: 100 },
        { processed_on: 10.days.ago, description: 'Also Old', amount: 10 },
        { processed_on: 20.days.from_now, description: 'irrelavant', amount: 10000 },
        { processed_on: 10.days.from_now, description: 'Might be new', amount: 10 }
      ])

      @reconciled_transaction = @account.trans.create({ 
        processed_on: 10.days.from_now, 
        description: 'Already Taken', 
        amount: 11.99 
      })

      @reconciled_bank_transaction = @reconciled_transaction.create_bank_transaction({ 
        bank_account: @account,
        processed_on: 10.days.from_now, 
        description: 'Already Taken', 
        amount: 11.99 
      })
    end

    it 'provides relevant suggestions' do
      @transactions = AddTransactionSuggestions.for_transactions(@account.bank_transactions, @account.trans)
      
      @old = @transactions.find {|t| t.description == 'Old'}
      expect(@old.suggestions.count).to eq 2

      @new = @transactions.find {|t| t.description == 'New'}
      expect(@new.suggestions.count).to eq 1
    end
  end
end