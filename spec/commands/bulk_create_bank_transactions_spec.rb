require 'rails_helper'

describe BulkCreateBankTransactions do
   describe '.from_fnb_csv' do
    CSV_LINES = [
      "2016/12/29, 2.00, -52338.96,RENT - SAN ROQUE",
      "2016/12/29, 50.00, -52338.96,Another Transaction"
    ]

    before :each do
      @account = BankAccount.create(name: 'FNB')
    end

    it 'creates a BankTransaction for each line' do
      expect {
        BulkCreateBankTransactions.from_fnb_csv(@account, CSV_LINES)
      }.to change { @account.bank_transactions.count }.from(0).to(2)

      expect(@account.bank_transactions.first).to have_attributes(
        processed_on: Date.parse("2016/12/29"),
        amount: BigDecimal.new(2),
        description: "RENT - SAN ROQUE"
      )
    end

    it 'prevents duplicates gracefully' do
      BulkCreateBankTransactions.from_fnb_csv(@account, CSV_LINES)
      expect {
        BulkCreateBankTransactions.from_fnb_csv(@account, CSV_LINES)
      }.not_to change { @account.bank_transactions.count }
    end
  end

  describe '.from_fnb_pending' do
    PENDING_LINES = [
      "05 Jan 2017 09:56:29",
      "400974******4036",
      "PHOTO FOCUS - WEST C",
      "50.00",
      "05 Jan 2017 13:07:25",
      "400974******4036","BP PARK DRIVE",
      "584.12"
    ]

    before :each do
      @account = BankAccount.create(name: 'FNB')
    end

    it 'creates a BankTransaction for each pending transaction' do
      expect {
        BulkCreateBankTransactions.from_fnb_pending(@account, PENDING_LINES)
      }.to change { @account.bank_transactions.count }.from(0).to(2)

      expect(@account.bank_transactions.first).to have_attributes(
        processed_on: Date.parse("05 Jan 2017 09:56:29"),
        amount: BigDecimal.new("50.00"),
        description: "PHOTO FOCUS - WEST C"
      )
    end



    it 'prevents duplicates gracefully' do
      BulkCreateBankTransactions.from_fnb_pending(@account, PENDING_LINES)
      expect {
        BulkCreateBankTransactions.from_fnb_pending(@account, PENDING_LINES)
      }.not_to change { @account.bank_transactions.count }
    end
  end 
end