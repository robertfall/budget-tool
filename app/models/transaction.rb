class Transaction < ApplicationRecord
  belongs_to :bank_account
  has_one :bank_transaction
  has_many :expense_allocation_transactions
  has_many :expenses, through: :expense_allocation_transactions
  accepts_nested_attributes_for :expense_allocation_transactions,
                                reject_if: :all_blank,
                                allow_destroy: true

  attr_reader :expense
  attr_accessor :suggestions

  def expense=(value)
    @expense = ActiveRecord::Type::Boolean.new.deserialize(value)
  end

  def reconciled?
    bank_transaction.present?
  end
end
