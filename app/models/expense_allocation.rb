class ExpenseAllocation < ApplicationRecord
  belongs_to :bank_account
  belongs_to :expense
  has_many :expense_allocation_transactions
  has_many :trans, through: :expense_allocation_transactions

  scope :for_bank_account, -> (bank_account) { where(bank_account: bank_account) }
  scope :until, -> (end_date) { where allocate_on: Date.today...end_date }
  scope :with_remaining_balance, -> { 
    joins('JOIN expense_allocation_balances eab ON eab.id = expense_allocations.id')
      .where('eab.amount > 0')
  }

  delegate :name, to: :expense

  def balance
   amount - spent
  end

  def spent
    -trans.sum(:amount)
  end
end
