class ExpenseAllocationTransaction < ApplicationRecord
  belongs_to :trans, class_name: 'Transaction', foreign_key: 'transaction_id', optional: true
  belongs_to :expense_allocation
  delegate :name, to: :expense_allocation
end
