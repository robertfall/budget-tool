class BankTransaction < ApplicationRecord
  belongs_to :trans, class_name: 'Transaction', foreign_key: 'transaction_id', optional: true
  belongs_to :bank_account

  attr_accessor :suggestions

  scope :starting_from, -> (starting_from) { 
    where('bank_transactions.processed_on > ?', starting_from)
      .order('processed_on DESC, id');
  }
  
  def reconciled?
    trans.present?
  end

  def reconcile!(params)
    BankTransaction.transaction do
      update(params) and trans.update(amount: self.amount)
    end
  end
end
