class BankAccount < ApplicationRecord
  has_many :expense_allocations
  has_many :statement_lines
  has_many :bank_transactions
  has_many :trans, class_name: 'Transaction', inverse_of: :bank_account
end
