class StatementLine < ApplicationRecord
  belongs_to :bank_account
  self.primary_key = :id
  has_many :expense_allocation_transactions, 
    inverse_of: :trans, 
    foreign_key: :transaction_id

  belongs_to :trans, 
    -> { where(statement_lines: {class_name: 'Transaction'}) },
    foreign_key: 'id', 
    class_name: 'Transaction'


  def reconciled?
    trans and trans.reconciled?
  end

  scope :starting_from, -> (starting_from) { where('statement_lines.processed_on > ?', starting_from) }
end
