class AddTransactionSuggestions
  def self.for_bank_transactions(transactions, bank_transactions)
    return if bank_transactions.empty?
    minimum_date = bank_transactions.minimum(:processed_on) - 5.days
    maximum_date = bank_transactions.maximum(:processed_on) + 5.days

    all_transactions = transactions
      .includes(:bank_transaction)
      .where("processed_on >= ? AND processed_on <= ?", minimum_date, maximum_date)
      .to_a

    bank_transactions.includes(:trans).tap do |bts|
      bts.each do |bt|
        bt.suggestions = suggestions_for_bank_transaction(bt, all_transactions)
      end
    end
  end

  def self.suggestions_for_bank_transaction(bank_transaction, transactions)
    valid_dates = bank_transaction.processed_on - 5.days...bank_transaction.processed_on + 5.days
    valid_amounts = bank_transaction.amount - 10 ... bank_transaction.amount + 10

    transactions.select do |t|
      valid_dates.cover?(t.processed_on) and
      valid_amounts.cover?(t.amount) and
      !t.reconciled?
    end
  end

  def self.for_transactions(bank_transactions, transactions)
    minimum_date = transactions.minimum(:processed_on) - 5.days
    maximum_date = transactions.maximum(:processed_on) + 5.days

    all_transactions = bank_transactions
      .includes(:trans)
      .where("processed_on >= ? AND processed_on <= ?", minimum_date, maximum_date)
      .to_a

    transactions.tap do |ts|
      ts.each do |ts|
        ts.suggestions = suggestions_for_transaction(ts, all_transactions)
      end
    end
  end

  def self.suggestions_for_transaction(transaction, bank_transactions)
    valid_dates = transaction.processed_on - 5.days...transaction.processed_on + 5.days
    valid_amounts = transaction.amount - 10 ... transaction.amount + 10

    bank_transactions.select do |bt|
      valid_dates.cover?(bt.processed_on) and
      valid_amounts.cover?(bt.amount) and
      !bt.reconciled?
    end
  end
end