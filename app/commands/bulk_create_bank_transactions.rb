class BulkCreateBankTransactions
  def self.from_fnb_csv(account, lines)
    lines.map { |l| from_fnb_csv_line(l) }.each do |trans|
      account.bank_transactions.find_or_create_by(trans)
    end
  end

  def self.from_fnb_pending(account, lines)
    lines.each_slice(4).map { |l| from_fnb_pending_group(l) }.each do |trans|
      account.bank_transactions.find_or_create_by(trans)
    end
  end
  
  private
  def self.from_fnb_csv_line(line)
    trans = line.split(',')
    {
      processed_on: trans[0], 
      amount: trans[1], 
      description: trans.last
    }
  end

    def self.from_fnb_pending_group(group)
    {
      processed_on: group[0], 
      amount: -group[3].to_f, 
      description: group[2]
    }
  end
end