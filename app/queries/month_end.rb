class MonthEnd
    def self.next(day_of_month)
        if (Date.today.change(day: day_of_month).month == Date.today.month)
            Date.today.change(day: day_of_month) + 1.month
        else
            Date.today.change(date: day_of_month)
        end
    end
end