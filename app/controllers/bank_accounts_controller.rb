class BankAccountsController < AuthenticatedController
  before_action :set_bank_account, only: [:show, :edit, :update, :destroy]
  before_action :set_starting_from, only: :show

  def index
    @bank_accounts = BankAccount.all
  end

  def show
    @statement_lines = @bank_account
      .statement_lines
      .includes(trans: :bank_transaction, expense_allocation_transactions: { expense_allocation: :expense} )
      .starting_from(@starting_from)
      .references(:transactions)
      .order('statement_lines.processed_on, order_weight, statement_lines.amount')
      .reverse
    
    @upcoming_allocations = @bank_account
      .expense_allocations
      .includes(:expense)
      .with_remaining_balance
      .until(MonthEnd.next(@bank_account.month_end))
      .select("expense_allocations.*, eab.amount as remaining")
      .to_a
      .uniq(&:name)
  end

  def new
    @bank_account = BankAccount.new
  end

  def edit
  end

  def create
    @bank_account = BankAccount.new(bank_account_params)

    respond_to do |format|
      if @bank_account.save
        format.html { redirect_to bank_accounts_path, notice: 'Bank Account was successfully created.' }
        format.json { render :show, status: :created, location: @bank_account }
      else
        format.html { render :new }
        format.json { render json: @bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

def update
    respond_to do |format|
      if @bank_account.update(bank_account_params)
        format.html { redirect_to bank_accounts_path, notice: 'Bank Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @bank_account }
      else
        format.html { render :edit }
        format.json { render json: @bank_account.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @bank_account.destroy
    respond_to do |format|
      format.html { redirect_to bank_accounts_url, notice: 'BankAccount was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_bank_account
      @bank_account = BankAccount.find(params[:id])
    end

    def set_starting_from
      @starting_from = Date.parse(params[:starting_from]) rescue Date.today - 7.days
    end

    def bank_account_params
      params.require(:bank_account).permit(:name, :credit, :overdraft)
    end
end
