class BankTransactionsController < ApplicationController
  before_action :set_bank_transaction, only: [:show, :edit, :update, :destroy, :reconcile]
  before_action :set_bank_account
  before_action :set_starting_from, only: :index

  def index
    bank_transactions = @bank_account.bank_transactions.starting_from(@starting_from)
    @bank_transactions = AddTransactionSuggestions
      .for_bank_transactions(@bank_account.trans, bank_transactions)
  end

  def show
  end

  def new
    @bank_transaction = BankTransaction.new
  end

  def edit
  end

  def create
    @bank_transaction = BankTransaction.new(bank_transaction_params)

    respond_to do |format|
      if @bank_transaction.save
        format.html { redirect_to @bank_transaction, notice: 'Bank transaction was successfully created.' }
        format.json { render :show, status: :created, location: @bank_transaction }
      else
        format.html { render :new }
        format.json { render json: @bank_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @bank_transaction.update(bank_transaction_params)
        format.html { redirect_to bank_account_bank_transactions_path(@bank_account), notice: 'Bank transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: [@bank_account, @bank_transaction] }
      else
        format.html { render :edit }
        format.json { render json: @bank_transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  def reconcile
    respond_to do |format|
      if @bank_transaction.reconcile!(reconcile_params)
        format.js { render 'bank_transactions/show_reconciled' }
      else
        @bank_transaction
      end
    end
  end

  def destroy
    @bank_transaction.destroy
    respond_to do |format|
      format.html { redirect_to bank_transactions_url, notice: 'Bank transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def import
    BulkCreateBankTransactions.from_fnb_csv(@bank_account, import_params[:data].lines)
    redirect_to bank_account_bank_transactions_url(@bank_account), notice: 'Transactions successfully imported'
  end

  private
    def set_bank_transaction
      @bank_transaction = BankTransaction.find(params[:id])
    end

    def set_bank_account
      @bank_account = BankAccount.find(params[:bank_account_id])
    end

    def set_starting_from
      @starting_from = Date.parse(params[:starting_from]) rescue Date.today - 7.days
    end

    def bank_transaction_params
      params.require(:bank_transaction).permit(:processed_on, :description, :amount)
    end

    def import_params
      params.permit(:data)
    end

    def reconcile_params
      params.require(:bank_transaction).permit(:transaction_id)
    end
end
