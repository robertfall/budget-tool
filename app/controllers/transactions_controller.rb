class TransactionsController < AuthenticatedController
  before_action :set_bank_account
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :set_bank_transaction, only: :create

  def show
  end

  def new
    @transaction = @bank_account.trans.new(processed_on: Date.today, expense: true)
    if params[:expense_allocation_id]
      @expense_allocation = ExpenseAllocation.find(params[:expense_allocation_id])
      @transaction.description = @expense_allocation.name
      @transaction.expense_allocation_transactions.build(expense_allocation: @expense_allocation)
    end
  end

  def edit
    @suggestions = AddTransactionSuggestions.suggestions_for_transaction(@transaction, @bank_account.bank_transactions)
  end

  def create
    @transaction = @bank_account.trans.build(transaction_params)
    @transaction.bank_transaction = @bank_transaction
    invert_sign(@transaction)
    
    respond_to do |format|
      if @transaction.save
        format.html { redirect_to @bank_account, notice: 'Transaction was successfully created.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @bank_account, notice: 'Transaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to @bank_account, notice: 'Transaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    def set_bank_account
      @bank_account = BankAccount.find(params[:bank_account_id])
    end

    def set_bank_transaction
      @bank_transaction = @bank_account.bank_transactions.find(params[:bank_transaction_id]) if params.key?(:bank_transaction_id)
    end

    def invert_sign(transaction)
      transaction.amount = -transaction.amount.abs if transaction.expense == true
    end

    def transaction_params
      params
        .require(:transaction)
        .permit(
          :description, 
          :amount,
          :processed_on,
          :expense,
          :bank_account_id,
          expense_allocation_transactions_attributes: [
            :id, :_destroy, :amount, :expense_allocation_id
          ]
      )
    end
end
