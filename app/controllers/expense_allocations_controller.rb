class ExpenseAllocationsController < AuthenticatedController
  before_action :set_bank_account
  before_action :set_expense_allocation, only: [:show, :edit, :update, :destroy]

  def new
    @expense_allocation = @bank_account.expense_allocations.build(allocate_on: Date.today)
  end

  def edit
  end

  def create
    @expense_allocation = @bank_account.expense_allocations.build(expense_allocation_params)
    respond_to do |format|
      if @expense_allocation.save
        format.html { redirect_to @bank_account, notice: 'Expense Allocation was successfully created.' }
        format.json { render :show, status: :ok, location: @expense_allocation }
      else
        format.html { render :edit }
        format.json { render json: @expense_allocation.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @expense_allocation.update(expense_allocation_params)
        format.html { redirect_to @bank_account, notice: 'Expense Allocation was successfully updated.' }
        format.json { render :show, status: :ok, location: @expense_allocation }
      else
        format.html { render :edit }
        format.json { render json: @expense_allocation.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @expense_allocation.destroy
    respond_to do |format|
      format.html { redirect_to @bank_account, notice: 'Expense Allocation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_expense_allocation
      @expense_allocation = @bank_account.expense_allocations.find(params[:id])
    end

    def set_bank_account
      @bank_account = BankAccount.find(params[:bank_account_id])
    end

    def expense_allocation_params
      params.require(:expense_allocation).permit(:expense_id, :amount, :allocate_on)
    end
end
