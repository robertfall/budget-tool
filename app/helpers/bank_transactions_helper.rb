module BankTransactionsHelper
  def bank_transaction_class(transaction, klass='')
    classes = [klass, 'statement-line', 'transaction']

    if transaction.reconciled?
      classes << 'reconciled'
    else
      classes << 'unreconciled'
    end

    if transaction.amount.positive? 
      classes << 'positive'
    else
      classes << 'negative'
    end
  end
end
