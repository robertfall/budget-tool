module BankAccountsHelper
 def render_statement_line(line)
    case line.class_name
    when 'Transaction'
      render('transaction', transaction: line)
    when 'ExpenseAllocation'
      render('allocation', allocation: line)
    end
  end

  def statement_line_class(line, klass='')
    classes = [klass, 'statement-line']

    classes << 'statement-line-red' if (line.respond_to?(:available) and line.available) <= 0
    if line.reconciled?
      classes << 'reconciled'
    else
      classes << 'unreconciled'
    end

    classes.join(' ')
  end
end
