FROM ruby:2.3.3-alpine

ARG DOCKER_USER_ID
ARG DOCKER_USER_NAME
RUN adduser -D -u $DOCKER_USER_ID $DOCKER_USER_NAME

ADD Gemfile* /app/

RUN apk --update add nodejs postgresql-dev

RUN apk --update add --virtual build-dependencies build-base ruby-dev openssl-dev \
    libc-dev linux-headers && \
    gem install bundler && \
    su $DOCKER_USER_NAME &&\
    cd /app ; bundle install && \
    exit && \
    apk del build-dependencies

USER $DOCKER_USER_NAME

WORKDIR /app
EXPOSE 3000
