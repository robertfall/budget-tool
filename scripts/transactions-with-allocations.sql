WITH all_transactions AS 
    (SELECT t.processed_on,
         t.description,
         t.amount,
         t.account_id
    FROM transactions t
    UNION
    SELECT cast(date_trunc('MONTH', cg.month) AS date) + a.allocate_on * interval '1' day, c.name, 
        (SELECT cg.amount - ABS(SUM(t2.amount))
        FROM transactions t2
        JOIN transaction_categories tc
            ON t2.id = tc.transaction_id
        WHERE tc.category_allocation_id = cg.id ) AS amount, cg.account_id
        FROM category_allocations cg
        JOIN categories c
            ON c.id = cg.category_id
        JOIN accounts a
            ON a.id = cg.account_id
        ORDER BY  processed_on )
    SELECT t.*,
         a.overdraft + t.balance AS available
FROM 
    (SELECT *,
         sum(amount)
        OVER ( PARTITION BY account_id
    ORDER BY  processed_on ROWS UNBOUNDED PRECEDING ) AS balance
    FROM all_transactions ) t
JOIN accounts a
    ON a.id = t.account_id;