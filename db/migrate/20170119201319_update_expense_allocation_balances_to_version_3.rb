class UpdateExpenseAllocationBalancesToVersion3 < ActiveRecord::Migration
  def change
    replace_view :expense_allocation_balances, version: 3, revert_to_version: 2
  end
end
