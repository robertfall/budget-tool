class CreateBankTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :bank_transactions do |t|
      t.date :processed_on
      t.string :description
      t.decimal :amount
      t.references :transaction, foreign_key: true
      t.references :bank_account, foreign_key: true, null: false

      t.timestamps
    end

    add_index :bank_transactions, 
      [:processed_on, :description, :amount], 
      name: 'bank_transactions_uniq', unique: true
  end
end
