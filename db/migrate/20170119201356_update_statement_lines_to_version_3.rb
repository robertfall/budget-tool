class UpdateStatementLinesToVersion3 < ActiveRecord::Migration
  def change
    replace_view :statement_lines, version: 3, revert_to_version: 2
  end
end
