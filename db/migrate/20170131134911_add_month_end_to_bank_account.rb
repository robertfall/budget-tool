class AddMonthEndToBankAccount < ActiveRecord::Migration[5.0]
  def change
    add_column :bank_accounts, :month_end, :integer, null: false, default: 27
  end
end
