class UpdateStatementLinesToVersion4 < ActiveRecord::Migration
  def change
    update_view :statement_lines, version: 4, revert_to_version: 3
  end
end
