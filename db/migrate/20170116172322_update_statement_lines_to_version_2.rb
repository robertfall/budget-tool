class UpdateStatementLinesToVersion2 < ActiveRecord::Migration
  def change
    update_view :statement_lines, version: 2, revert_to_version: 1
  end
end
