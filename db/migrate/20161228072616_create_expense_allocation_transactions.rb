class CreateExpenseAllocationTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :expense_allocation_transactions do |t|
      t.references :transaction, foreign_key: true, null: false
      t.references :expense_allocation, foreign_key: true, null: false
      t.decimal :amount

      t.timestamps
    end
  end
end
