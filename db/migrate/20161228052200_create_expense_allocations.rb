class CreateExpenseAllocations < ActiveRecord::Migration[5.0]
  def change
    create_table :expense_allocations do |t|
      t.references :bank_account, foreign_key: true, null: false
      t.references :expense, foreign_key: true, null: false
      t.decimal :amount, null: false
      t.date :allocate_on, null: false
      t.timestamps
    end
  end
end
