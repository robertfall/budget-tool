class CreateExpenseAllocationBalances < ActiveRecord::Migration[5.0]
  def change
    create_view :expense_allocation_balances
  end
end
