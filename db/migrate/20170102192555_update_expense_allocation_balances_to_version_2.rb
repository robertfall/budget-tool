class UpdateExpenseAllocationBalancesToVersion2 < ActiveRecord::Migration
  def change
    replace_view :expense_allocation_balances, version: 2, revert_to_version: 1
  end
end
