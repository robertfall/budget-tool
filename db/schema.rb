# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170613173318) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ar_internal_metadata", primary_key: "key", id: :string, force: :cascade do |t|
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "name"
    t.decimal  "credit"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "month_end",  default: 27, null: false
  end

  create_table "bank_transactions", force: :cascade do |t|
    t.date     "processed_on"
    t.string   "description"
    t.decimal  "amount"
    t.integer  "transaction_id"
    t.integer  "bank_account_id", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["bank_account_id"], name: "index_bank_transactions_on_bank_account_id", using: :btree
    t.index ["processed_on", "description", "amount"], name: "bank_transactions_uniq", using: :btree
    t.index ["transaction_id"], name: "index_bank_transactions_on_transaction_id", using: :btree
  end

  create_table "expense_allocation_transactions", force: :cascade do |t|
    t.integer  "transaction_id",        null: false
    t.integer  "expense_allocation_id", null: false
    t.decimal  "amount"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["expense_allocation_id"], name: "index_expense_allocation_transactions_on_expense_allocation_id", using: :btree
    t.index ["transaction_id"], name: "index_expense_allocation_transactions_on_transaction_id", using: :btree
  end

  create_table "expense_allocations", force: :cascade do |t|
    t.integer  "bank_account_id", null: false
    t.integer  "expense_id",      null: false
    t.decimal  "amount",          null: false
    t.date     "allocate_on",     null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["bank_account_id"], name: "index_expense_allocations_on_bank_account_id", using: :btree
    t.index ["expense_id"], name: "index_expense_allocations_on_expense_id", using: :btree
  end

  create_table "expenses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "description"
    t.date     "processed_on"
    t.integer  "bank_account_id"
    t.decimal  "amount"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["bank_account_id"], name: "index_transactions_on_bank_account_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "email",                          null: false
    t.string   "encrypted_password", limit: 128, null: false
    t.string   "confirmation_token", limit: 128
    t.string   "remember_token",     limit: 128, null: false
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["remember_token"], name: "index_users_on_remember_token", using: :btree
  end

  add_foreign_key "bank_transactions", "bank_accounts"
  add_foreign_key "bank_transactions", "transactions"
  add_foreign_key "expense_allocation_transactions", "expense_allocations"
  add_foreign_key "expense_allocation_transactions", "transactions"
  add_foreign_key "expense_allocations", "bank_accounts"
  add_foreign_key "expense_allocations", "expenses"
  add_foreign_key "transactions", "bank_accounts"

  create_view :expense_allocation_balances,  sql_definition: <<-SQL
      WITH expense_allocation_sums AS (
           SELECT ea_1.id AS expense_allocation_id,
              GREATEST((0)::numeric, sum(eat.amount)) AS spent,
              (ea_1.amount - GREATEST((0)::numeric, sum(eat.amount))) AS remaining
             FROM (expense_allocations ea_1
               LEFT JOIN expense_allocation_transactions eat ON ((eat.expense_allocation_id = ea_1.id)))
            GROUP BY ea_1.id
          )
   SELECT ea.id,
      ea.allocate_on AS processed_on,
      e.name AS description,
      eas.remaining AS amount,
      ea.bank_account_id
     FROM (((expense_allocations ea
       JOIN expense_allocation_sums eas ON ((eas.expense_allocation_id = ea.id)))
       JOIN expenses e ON ((e.id = ea.expense_id)))
       JOIN bank_accounts a ON ((a.id = ea.bank_account_id)));
  SQL

  create_view :statement_lines,  sql_definition: <<-SQL
      WITH entries AS (
           SELECT t.id,
              t.processed_on,
              t.description,
              t.amount,
              t.bank_account_id,
              'Transaction'::text AS class_name,
              (bt.id IS NOT NULL) AS reconciled,
              1 AS order_weight
             FROM (transactions t
               LEFT JOIN bank_transactions bt ON ((bt.transaction_id = t.id)))
          UNION
           SELECT expense_allocation_balances.id,
              expense_allocation_balances.processed_on,
              expense_allocation_balances.description,
              (- expense_allocation_balances.amount),
              expense_allocation_balances.bank_account_id,
              'ExpenseAllocation'::text AS class_name,
              false AS reconciled,
              0 AS order_weight
             FROM expense_allocation_balances
            WHERE (expense_allocation_balances.amount > (0)::numeric)
    ORDER BY 2
          )
   SELECT e.id,
      e.processed_on,
      e.description,
      e.amount,
      e.bank_account_id,
      e.class_name,
      e.reconciled,
      e.order_weight,
      e.balance,
      (a.credit + e.balance) AS available
     FROM (( SELECT entries.id,
              entries.processed_on,
              entries.description,
              entries.amount,
              entries.bank_account_id,
              entries.class_name,
              entries.reconciled,
              entries.order_weight,
              sum(entries.amount) OVER (PARTITION BY entries.bank_account_id ORDER BY entries.processed_on, entries.order_weight, entries.amount ROWS UNBOUNDED PRECEDING) AS balance
             FROM entries) e
       JOIN bank_accounts a ON ((a.id = e.bank_account_id)));
  SQL

end
