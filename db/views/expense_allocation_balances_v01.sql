WITH expense_allocation_sums AS 
    (SELECT ea.id AS expense_allocation_id,
         GREATEST(0,
         ABS(SUM(t.amount))) AS spent,
         ea.amount - GREATEST(0,
         ABS(SUM(t.amount))) AS remaining
    FROM expense_allocations ea
    LEFT JOIN expense_allocation_transactions eat
        ON eat.expense_allocation_id = ea.id
    LEFT JOIN transactions t
        ON t.id = eat.transaction_id
    GROUP BY  ea.id )
SELECT ea.id,
         ea.allocate_on AS processed_on,
         e.name AS description,
         eas.remaining AS amount,
         ea.bank_account_id
FROM expense_allocations ea
JOIN expense_allocation_sums eas
    ON eas.expense_allocation_id = ea.id
JOIN expenses e
    ON e.id = ea.expense_id
JOIN bank_accounts a
    ON a.id = ea.bank_account_id