WITH entries AS 
    (SELECT id,
         processed_on,
         description,
         amount,
         bank_account_id,
         'Transaction' as class_name
    FROM transactions
    UNION
    SELECT id,
         processed_on,
         description,
         -amount,
         bank_account_id,
         'ExpenseAllocation' as class_name
    FROM expense_allocation_balances
    ORDER BY  processed_on)
SELECT e.*,
         a.credit + e.balance AS available
FROM 
    (SELECT *,
         sum(amount)
        OVER ( PARTITION BY bank_account_id
    ORDER BY  processed_on ROWS UNBOUNDED PRECEDING ) AS balance
    FROM entries ) e
JOIN bank_accounts a
    ON a.id = e.bank_account_id