WITH entries AS 
    (SELECT t.id,
         t.processed_on,
         t.description,
         t.amount,
         t.bank_account_id,
         'Transaction' AS class_name,
         bt.id IS NOT NULL AS reconciled,
         1 as order_weight
    FROM transactions t
    LEFT JOIN bank_transactions bt
        ON bt.transaction_id = t.id
    UNION
    SELECT id,
         processed_on,
         description,
         -amount,
         bank_account_id,
         'ExpenseAllocation' AS class_name,
         false as reconciled,
         0 as order_weight
    FROM expense_allocation_balances
    WHERE amount > 0
    ORDER BY  processed_on)
SELECT e.*,
         a.credit + e.balance AS available
FROM 
    (SELECT *,
         sum(amount)
        OVER ( PARTITION BY bank_account_id
    ORDER BY  processed_on, order_weight, amount ROWS UNBOUNDED PRECEDING ) AS balance
    FROM entries ) e
JOIN bank_accounts a
    ON a.id = e.bank_account_id ;