Rails.application.routes.draw do
  root to: 'bank_accounts#index'
  resources :expenses
  resources :bank_accounts do
    resources :expense_allocations
    resources :transactions, except: [:index, :show]
    resources :bank_transactions do
      post 'import', on: :collection
      patch 'reconcile', on: :member
    end
  end
  get '/sign_out', to: 'clearance/sessions#destroy', as: 'sign_out_get'
end
